<!--
SPDX-FileCopyrightText: Huawei Inc.

SPDX-License-Identifier: CC-BY-4.0
-->

# meta-oniro-blueprints-gateway
Welcome to the Oniro Blueprint for a Transparent Gateway.

Oniro is an Eclipse Foundation project focused on the development of a
distributed open source operating system for consumer devices.

*\*Oniro is a trademark of Eclipse Foundation.*

In this repository you can find the necessary yocto layers to run the 
Transparent Gateway blueprint.

Documentation for this blueprint can be found on the [Oniro Project Blueprints 
documentation page](https://docs.oniroproject.org/projects/blueprints/transparent-gateway.html).

## Set up your workspace
The following instructions shows how to set up the Oniro Project workspace, 
please visit the [Oniro docs](
https://docs.oniroproject.org/en/latest/oniro/oniro-quick-build.html
) for more details.
```console
$ mkdir ~/oniroproject; cd ~/oniroproject
$ repo init -u https://gitlab.eclipse.org/eclipse/oniro-core/oniro.git -b kirkstone
$ repo sync --no-clone-bundle
$ TEMPLATECONF=../oniro/flavours/linux . ./oe-core/oe-init-build-env build-oniro-linux
```

## Add the Transparent Gateway blueprint layers to your build
Clone this repository into your workspace
```console 
$ git clone --recurse-submodules https://gitlab.eclipse.org/eclipse/oniro-blueprints/transparent-gateway/meta-oniro-blueprints-gateway.git ~/oniroproject/meta-oniro-blueprints-gateway
```

Add the layers to your yocto build
```console
$ bitbake-layers add-layer ~/oniroproject/meta-oniro-blueprints-gateway/meta-oniro-blueprints-core
$ bitbake-layers add-layer ~/oniroproject/meta-oniro-blueprints-gateway/meta-oniro-blueprints-gateway
```

## Build and run a linux target

Build the Oniro image with the Transparent Gateway blueprint 
```console
$ DISTRO=oniro-linux-blueprint-gateway MACHINE=<your board> bitbake blueprint-gateway-image
```

# Contributing

## Merge requests

All contributions are to be handled as merge requests in the
[meta-oniro-blueprints-gateway Gitlab repository](
https://gitlab.eclipse.org/eclipse/oniro-blueprints/transparent-gateway/meta-oniro-blueprints-gateway
). For
more information on the contributing process, check the `CONTRIBUTING.md` file.

## Maintainers

* Andrei Gherzan <andrei.gherzan@huawei.com>
* Stefan Schmidt <stefan.schmidt@huawei.com>
* Francesco Pham <francesco.pham@huawei.com>

# License

This layer is release under the licenses listed in the `LICENSES` root directory.
